const int buttonPinPrev = 4;     // the number of the pushbutton pin
const int buttonPinNext = 5;
//const int ledPin =  13;  // the number of the LED pin
const int mainLedPin = 8;
const int prevPageMsg = 1;
const int nextPageMsg = 2;

const int loopDelay = 250;


#include <SoftwareSerial.h>
SoftwareSerial BTserial(3, 2); // RX | TX on blutoot RXD to 2, TXD - 3

const long BTbaudRate = 9600; 



 
void setup() 
{
    Serial.begin(9600);
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(" ");
 
    BTserial.begin(BTbaudRate);  
    
    
    Serial.print("BTserial started at "); Serial.println(BTbaudRate);
    Serial.println(" ");

    pinMode(buttonPinPrev, INPUT_PULLUP);
    pinMode(buttonPinNext, INPUT_PULLUP);
    pinMode(mainLedPin, OUTPUT);
    Serial.println("jedem");
}

void ButtonPresed(int blueMsg) {
    digitalWrite(mainLedPin, HIGH);
   // BTserial.write(blueMsg);
   //BTserial.flush();
    
   // BTserial.println("AT\r\n");
    //BTserial.print(blueMsg);
    //BTserial.flush();
    //char buffer[40];
    //sprintf(buffer, "presed pin %d - bluetoot msg = %d", pinPort, blueMsg);
     Serial.print("trying to send=");
    if(blueMsg==1) {
      Serial.print("prev");
      sendBT("prev", 4);
    } else {
      Serial.print("next");
      sendBT("next", 4);
    }

    Serial.println(" ");
    delay(loopDelay);
    //digitalWrite(mainLedPin, LOW);
}

void sendBT(const char *data, int l)
{
  byte len[4];
  len[0] = 85; //preamble
  len[1] = 85; //preamble
  len[2] = (l >> 8) & 0x000000FF;
  len[3] = (l & 0x000000FF);
  BTserial.write(len, 4);
  BTserial.flush();

  //Serial.print(String(data) );
  Serial.write(data);
  Serial.println();
  BTserial.write(data, l);
  BTserial.flush();
}
 
void loop()
{
  int buttonStatePrev = digitalRead(buttonPinPrev);
  int buttonStateNext = digitalRead(buttonPinNext);
  //print out the value of the pushbutton
 // delay(1000);
/*
  long randomNumber = random(0,2);
  Serial.print("randomNumber=");
  Serial.print(randomNumber);
  Serial.println(" ");
  if(randomNumber==0) {
    sendBT("prev", 4);
    //ButtonPresed(prevPageMsg);
  } else {
    sendBT("next", 4);
    //ButtonPresed(nextPageMsg);
  }*/
  digitalWrite(mainLedPin, LOW);
  
  // Keep in mind the pull-up means the pushbutton's logic is inverted. It goes
  // HIGH when it's open, and LOW when it's pressed. Turn on pin 13 when the
  // button's pressed, and off when it's not:
  
  if (buttonStatePrev == HIGH) {
    //sendBT("next", 4);
    ButtonPresed(prevPageMsg);

  }
  if (buttonStateNext == HIGH) {
    ButtonPresed(nextPageMsg);

  }



  
  /* // Serial.println("not presed");
 
    // Read from the Serial Monitor and send to the Bluetooth module
    if (Serial.available())
    {
        c = Serial.read();
        BTserial.write(c);   
 
        // Echo the user input to the main window. The ">" character indicates the user entered text.
       // if (NL) { Serial.print(">");  NL = false; }
        Serial.write(c);
        //if (c==10) { NL = true; }
    }*/
 
}
