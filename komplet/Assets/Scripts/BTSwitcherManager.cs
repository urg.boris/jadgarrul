﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArduinoBluetoothAPI;
using System;
using System.Text;
using Paroxe.PdfRenderer;
using UnityEngine.Serialization;

public class BTSwitcherManager : MonoBehaviour
{
    // Use this for initialization
    BluetoothHelper bluetoothHelper;
    BluetoothHelper bluetoothHelper2;
    string deviceName;
    private const string NEXT_COMMAND = "next";
    private const string PREV_COMMAND = "prev";
    public Text text;

    public AppManager AppManager;
    //[FormerlySerializedAs("pdfViewer")] public PDFViewer pdfViewer1;
   // public PDFViewer pdfViewer2;
   // private int currentDobblePageIndex = 0;
    private bool keepCheckingConnection;
    private Coroutine _coroutine;
    public Image btCOMImage;

    private WaitForSeconds loopRate = new WaitForSeconds(7);

    string received_message;
    private WaitForSeconds _blinkForSeconds = new WaitForSeconds(0.5f);

    void Start()
    {
        RunConnectSequence();
        keepCheckingConnection = true;
        _coroutine = StartCoroutine(ConnectionCheckingLoop());
        text.gameObject.SetActive(false);
       // currentDobblePageIndex = 0;
    }

    public void RunConnectSequence()
    {
        deviceName = "HC-05"; //bluetooth should be turned ON;
        try
        {
            bluetoothHelper = BluetoothHelper.GetInstance(deviceName);
            bluetoothHelper.OnConnected += OnConnected;
            bluetoothHelper.OnConnectionFailed += OnConnectionFailed;
           // bluetoothHelper.OnDataReceived += OnMessageReceived; //read the data

            //bluetoothHelper.setFixedLengthBasedStream(1); //receiving every 3 characters together
            //bluetoothHelper.setTerminatorBasedStream("\n"); //delimits received messages based on \n char
            //if we received "Hi\nHow are you?"
            //then they are 2 messages : "Hi" and "How are you?"


            bluetoothHelper.setLengthBasedStream();
            /*
            will received messages based on the length provided, this is useful in transfering binary data
            if we received this message (byte array) :
            {0x55, 0x55, 0, 3, 'a', 'b', 'c', 0x55, 0x55, 0, 9, 'i', ' ', 'a', 'm', ' ', 't', 'o', 'n', 'y'}
            then its parsed as 2 messages : "abc" and "i am tony"
            the first 2 bytes are the length data writted on 2 bytes
            byte[0] is the MSB
            byte[1] is the LSB

            on the unity side, you dont have to add the message length implementation.

            if you call bluetoothHelper.SendData("HELLO");
            this API will send automatically :
             0x55 0x55    0x00 0x05   0x68 0x65 0x6C 0x6C 0x6F
            |________|   |________|  |________________________|
             preamble      Length             Data

            
            when sending data from the arduino to the bluetooth, there's no preamble added.
            this preamble is used to that you receive valid data if you connect to your arduino and its already send data.
            so you will not receive 
            on the arduino side you can decode the message by this code snippet:
            char * data;
            char _length[2];
            int length;

            if(Serial.avalaible() >2 )
            {
                _length[0] = Serial.read();
                _length[1] = Serial.read();
                length = (_length[0] << 8) & 0xFF00 | _length[1] & 0xFF00;

                data = new char[length];
                int i=0;
                while(i<length)
                {
                    if(Serial.available() == 0)
                        continue;
                    data[i++] = Serial.read();
                }


                ...process received data...


                delete [] data; <--dont forget to clear the dynamic allocation!!!
            }
            */

            LinkedList<BluetoothDevice> ds = bluetoothHelper.getPairedDevicesList();

            //AppendLog(ds);
            // if(bluetoothHelper.isDevicePaired())
            // 	btCOMImage.GetComponent<Renderer>().material.color = Color.blue;
            // else
            // 	btCOMImage.GetComponent<Renderer>().material.color = Color.grey;
        }
        catch (Exception ex)
        {
            btCOMImage.color = Color.yellow;
            AppendLog("init error");
            AppendLog(ex.Message);
            //BlueToothNotEnabledException == bluetooth Not turned ON
            //BlueToothNotSupportedException == device doesn't support bluetooth
            //BlueToothNotReadyException == the device name you chose is not paired with your android or you are not connected to the bluetooth device;
            //								bluetoothHelper.Connect () returned false;
        }

        Connect();
    }

    public void ToggleDebugText()
    {
        text.gameObject.SetActive(!text.gameObject.activeSelf);
    }

    IEnumerator ConnectionCheckingLoop()
    {
        while (keepCheckingConnection)
        {
            yield return loopRate;
            ReConnect();
        }
    }

    IEnumerator blinkSphere()
    {
        btCOMImage.color = Color.blue;
        yield return _blinkForSeconds;
        btCOMImage.color = Color.green;
    }

    public void AppendLog(string msg)
    {
        text.text = text.text + msg+"\n";
    }
/*
    public void ShowDobblePage(int dobblePageIndex)
    {
        currentDobblePageIndex = dobblePageIndex;
        int maxDobllePage = Mathf.CeilToInt((float)pdfViewer1.PageCount / (float)2);
        if (dobblePageIndex > maxDobllePage)
        {
            currentDobblePageIndex = maxDobllePage;
        }
        if (dobblePageIndex < 0 )
        {
            currentDobblePageIndex = 0;
        }
        int firstPageIndex = currentDobblePageIndex * 2;
        int secondPageIndex = firstPageIndex+1;
        Debug.Log("currentDobblePageIndex"+currentDobblePageIndex);
        bool isPageCountEven = pdfViewer1.PageCount % 2 == 0;
        pdfViewer1.gameObject.SetActive(true);
        pdfViewer2.gameObject.SetActive(true);

        Debug.Log("currentDobblePageIndex"+currentDobblePageIndex);
        Debug.Log("pages"+firstPageIndex+"  "+secondPageIndex);
        pdfViewer1.GoToPage(firstPageIndex);
        pdfViewer2.GoToPage(secondPageIndex);
    }*/

    public void NextPageCommand()
    {
        AppManager.NextPage();
        //currentDobblePageIndex++;
       // ShowDobblePage(currentDobblePageIndex);
        /*
        int nextEvenPageIndex = pdfViewer1.CurrentPageIndex + 2;
        int nextOddPageIndex = pdfViewer2.CurrentPageIndex + 2;

        if (nextOddPageIndex >= pdfViewer1.PageCount)
        {
            pdfViewer1.gameObject.SetActive(false);
        }
        
        
        
        if (nextEvenPageIndex >= pdfViewer1.PageCount)
        {
            pdfViewer1.gameObject.SetActive(false);
        }
        else
        {
            pdfViewer1.GoToPage(nextEvenPageIndex); 
        }
        Debug.Log("pgcount "+pdfViewer2.PageCount);
        int nextOddPageIndex = pdfViewer2.CurrentPageIndex + 2;
        if (nextOddPageIndex >= pdfViewer2.PageCount)
        {
            pdfViewer2.gameObject.SetActive(false);
        }
        else
        {
            pdfViewer2.GoToPage(nextOddPageIndex); 
        }*/
        // Debug.Log("show "+nextEvenPageIndex+" " +nextOddPageIndex);
        //pdfViewer1.GoToPage(pdfViewer1.CurrentPageIndex+2);

    }

    public void PrevPageCommand()
    {
        AppManager.PrevPage();
       /* currentDobblePageIndex--;
        ShowDobblePage(currentDobblePageIndex);*/
        /*
        int prevEvenPageIndex = pdfViewer1.CurrentPageIndex - 2;
        int prevOddPageIndex = pdfViewer2.CurrentPageIndex - 2;
        if (prevEvenPageIndex < 0)
        {
            prevEvenPageIndex = 0;
            prevOddPageIndex = 1;
        }
        Debug.Log("show "+prevEvenPageIndex+" " +prevOddPageIndex);
        pdfViewer1.gameObject.SetActive(true);
        pdfViewer2.gameObject.SetActive(true);
        pdfViewer1.GoToPage(prevEvenPageIndex);
        pdfViewer2.GoToPage(prevOddPageIndex);*/
    }

    // Update is called once per frame
    void Update()
    {
        //Synchronous method to receive messages
        if (bluetoothHelper != null)
        {
            if (bluetoothHelper.Available)
            {
                StartCoroutine(blinkSphere());
                received_message = bluetoothHelper.Read();
                if (received_message == NEXT_COMMAND)
                {
                    AppendLog("COMMAND:"+received_message);
                    NextPageCommand();

                }
                if (received_message == PREV_COMMAND)
                {
                    AppendLog("COMMAND:"+received_message);
                    PrevPageCommand();
                }
            }
        }

    }

    //Asynchronous method to receive messages
 /*   void OnMessageReceived()
    {
        // not working
        StartCoroutine(blinkSphere());
        received_message = bluetoothHelper.Read();
        Debug.Log(received_message);
        AppendLog(received_message);
        // Debug.Log(received_message);
        
    }*/
    
  /*  void OnMessageReceived2()
    {
        // not working
        StartCoroutine(blinkSphere());
        received_message = bluetoothHelper.Read();
        Debug.Log(received_message);
        AppendLog(received_message);
        // Debug.Log(received_message);
        
    }*/

    void Connect()
    {
        AppendLog("connected on load");
        if (bluetoothHelper.isDevicePaired())
            bluetoothHelper.Connect(); // tries to connect
        else
            btCOMImage.color = Color.magenta;
    }

    void ReConnect()
    {
        
        if (!bluetoothHelper.isConnected() && bluetoothHelper.isDevicePaired())
        {
            if (bluetoothHelper != null)
                bluetoothHelper.Disconnect();
            AppendLog("trying to reconnect..");
            RunConnectSequence();
        }
    }

    void OnConnected()
    {
        btCOMImage.color = Color.green;
        try
        {
            bluetoothHelper.StartListening();
            AppendLog("CONNECTED!");
/*
            bluetoothHelper2 = BluetoothHelper.GetNewInstance();
            bluetoothHelper2.OnScanEnded += ScanEnded2;
            bluetoothHelper2.ScanNearbyDevices();*/
        }
        catch (Exception ex)
        {
            AppendLog("OnConnected error");
            AppendLog(ex.Message);
        }
    }

    private void ScanEnded2(LinkedList<BluetoothDevice> devices)
    {
        AppendLog("devices.Count="+devices.Count);
    }

    void OnConnectionFailed()
    {
        btCOMImage.color = Color.red;
        AppendLog("Connection Failed");
        //ReConnect();
    }


    //Call this function to emulate message receiving from bluetooth while debugging on your PC.
   /* void OnGUI()
    {
        if (bluetoothHelper != null)
            bluetoothHelper.DrawGUI();
        else
            return;

        if (!bluetoothHelper.isConnected())
            if (GUI.Button(
                new Rect(Screen.width / 2 - Screen.width / 10, Screen.height / 10, Screen.width / 5,
                    Screen.height / 10), "Connect"))
            {
                if (bluetoothHelper.isDevicePaired())
                    bluetoothHelper.Connect(); // tries to connect
                else
                    btCOMImage.GetComponent<Renderer>().material.color = Color.magenta;
            }

        if (bluetoothHelper.isConnected())
            if (GUI.Button(
                new Rect(Screen.width / 2 - Screen.width / 10, Screen.height - 2 * Screen.height / 10, Screen.width / 5,
                    Screen.height / 10), "Disconnect"))
            {
                bluetoothHelper.Disconnect();
                btCOMImage.GetComponent<Renderer>().material.color = Color.blue;
            }

        if (bluetoothHelper.isConnected())
            if (GUI.Button(
                new Rect(Screen.width / 2 - Screen.width / 10, Screen.height / 10, Screen.width / 5,
                    Screen.height / 10), "Send text"))
            {
                bluetoothHelper.SendData(new Byte[] {0, 0, 85, 0, 85});
                // bluetoothHelper.SendData("This is a very long long long long text");
            }
    }*/

    void OnDestroy()
    {
        keepCheckingConnection = false;
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }

        bluetoothHelper?.Disconnect();
    }
}