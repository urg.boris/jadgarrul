﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Paroxe.PdfRenderer;
using SimpleFileBrowser;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ImageViewer : MonoBehaviour
{
    public ArrayList imageBuffer = new ArrayList();
    public RawImage image;
    
    public List<Texture2D> imageTextures = new List<Texture2D>();
    public int currentPage;
    public Text paging;

    public void LoadImagesInDir(string dirPath, string extension)
    {
        var filePaths = Directory.GetFiles(dirPath, "*" + extension, SearchOption.TopDirectoryOnly);
        CreateImageList(filePaths);
    }
    
    private void CreateImageList(string[] filePaths)
    {
        string pathPrefix = @"file://";
        var i=0;
        foreach (var filePath in filePaths)
        {
            string fullFilename = pathPrefix + filePath;
            WWW www = new WWW(fullFilename);
            Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.DXT1, false);
            //LoadImageIntoTexture compresses JPGs by DXT1 and PNGs by DXT5     
            www.LoadImageIntoTexture(texTmp);
            imageTextures.Add(texTmp);
            i++;
        }

        currentPage = 0;
        ShowCurrentPage();

    }

    public void ShowCurrentPage()
    {
        image.texture = imageTextures[currentPage];
        paging.text = (currentPage+1) + "/" + imageTextures.Count;
    }

    public void NextPage()
    {
        currentPage++;
        if (currentPage >= imageTextures.Count)
        {
            currentPage = imageTextures.Count-1;
        }

        ShowCurrentPage();
    } 
    public void PrevPage()
    {
        currentPage--;
        if (currentPage < 0)
        {
            currentPage = 0;
        }

        ShowCurrentPage();
    } 
}