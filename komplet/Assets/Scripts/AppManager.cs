using System.Collections;
using System.Collections.Generic;
using System.IO;
using Paroxe.PdfRenderer;
using SimpleFileBrowser;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    public PDFViewer PdfViewer1;
    public Canvas PdfViewerCanvas;
    public ImageViewer ImageViewer;
    public Canvas ImageViewerCanvas;
    
    //public string defaultPDFFile;
    private string PDFs_defaultDir  = "/storage/emulated/0/Download/Noty/";

    public BTSwitcherManager BtSwitcherManager;
    public bool IsPdf;
    public bool loaded = false;

    void Start()
    {
        PdfViewerCanvas.gameObject.SetActive(false);
        ImageViewerCanvas.gameObject.SetActive(false);
        
        OpenFileFinder();
    }

    public void OpenFileFinder()
    {

        var filter = new FileBrowser.Filter("Noty", ".jpg", ".jpeg", ".png", ".pdf");
        // Start is called before the first frame update
        FileBrowser.SetFilters(true, filter);

        // Set default filter that is selected when the dialog is shown (optional)
        // Returns true if the default filter is set successfully
        // In this case, set Images filter as the default filter
        FileBrowser.SetDefaultFilter(".jpg");

        // Set excluded file extensions (optional) (by default, .lnk and .tmp extensions are excluded)
        // Note that when you use this function, .lnk and .tmp extensions will no longer be
        // excluded unless you explicitly add them as parameters to the function
        //FileBrowser.SetExcludedExtensions( ".lnk", ".tmp", ".zip", ".rar", ".exe" );

        // Add a new quick link to the browser (optional) (returns true if quick link is added successfully)
        // It is sufficient to add a quick link just once
        // Name: Users
        // Path: C:\Users
        // Icon: default (folder icon)
  
        FileBrowser.AddQuickLink("Noty", PDFs_defaultDir, null);

        // Show a save file dialog 
        // onSuccess event: not registered (which means this dialog is pretty useless)
        // onCancel event: not registered
        // Save file/folder: file, Allow multiple selection: false
        // Initial path: "C:\", Initial filename: "Screenshot.png"
        // Title: "Save As", Submit button text: "Save"
        // FileBrowser.ShowSaveDialog( null, null, FileBrowser.PickMode.Files, false, "C:\\", "Screenshot.png", "Save As", "Save" );
/*
        FileBrowser.ShowLoadDialog( ( paths ) => { Debug.Log( "Selected: " + paths[0] ); },
            						   () => { Debug.Log( "Canceled" ); },
            						   FileBrowser.PickMode.Folders, false, null, null, "Select Folder", "Select" );
  */      
        // Show a select folder dialog 
        // onSuccess event: print the selected folder's path
        // onCancel event: print "Canceled"
        // Load file/folder: folder, Allow multiple selection: false
        // Initial path: default (Documents), Initial filename: empty
        // Title: "Select Folder", Submit button text: "Select"
        // FileBrowser.ShowLoadDialog( ( paths ) => { Debug.Log( "Selected: " + paths[0] ); },
        //						   () => { Debug.Log( "Canceled" ); },
        //						   FileBrowser.PickMode.Folders, false, null, null, "Select Folder", "Select" );

        // Coroutine example
        StartCoroutine(ShowLoadDialogCoroutine());
    }

    public void NextPage()
    {
        if (!loaded)
        {
            return;
        }

        if (IsPdf)
        {
            PdfViewer1.GoToNextPage();
        }
        else
        {
            ImageViewer.NextPage();
        }
    }

    public void PrevPage()
    {
        if (!loaded)
        {
            return;
        }
        if (IsPdf)
        {
            PdfViewer1.GoToPreviousPage();
        }
        else
        {
            ImageViewer.PrevPage();
        }
    }

    IEnumerator ShowLoadDialogCoroutine()
    {
        loaded = false;
        // Show a load file dialog and wait for a response from user
        // Load file/folder: both, Allow multiple selection: true
        // Initial path: default (Documents), Initial filename: empty
        // Title: "Load File", Submit button text: "Load"
        yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.FilesAndFolders, false, null, null,
            "Načti noty", "Ukaž!");

        // Dialog is closed
        // Print whether the user has selected some files/folders or cancelled the operation (FileBrowser.Success)
//        Debug.Log(FileBrowser.Success);

        if (FileBrowser.Success)
        {
            
            // Print paths of the selected files (FileBrowser.Result) (null, if FileBrowser.Success is false)
            //for (int i = 0; i < FileBrowser.Result.Length; i++)
                //Debug.Log(FileBrowser.Result[i]);

            // Read the bytes of the first file via FileBrowserHelpers
            // Contrary to File.ReadAllBytes, this function works on Android 10+, as well
            byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(FileBrowser.Result[0]);

            // Or, copy the first file to persistentDataPath
            /*  string destinationPath = Path.Combine(Application.persistentDataPath,
                  FileBrowserHelpers.GetFilename(FileBrowser.Result[0]));
              FileBrowserHelpers.CopyFile(FileBrowser.Result[0], destinationPath);*/
            string path = FileBrowser.Result[0].Replace('\\', '/');
            BtSwitcherManager.AppendLog("loading file="+path);
            var extension = Path.GetExtension(path);
            var dirPath = Path.GetDirectoryName(path);
//            Debug.Log(extension+ "  "+dirPath);
            IsPdf = extension == ".pdf";
            if (IsPdf)
            {
                PdfViewerCanvas.gameObject.SetActive(true);
                PdfViewer1.FilePath = path;
                PdfViewer1.LoadDocument();
            }
            else
            {
                ImageViewerCanvas.gameObject.SetActive(true);
                ImageViewer.LoadImagesInDir(dirPath, extension);
            }

            loaded = true;

            //  PdfViewer2.FilePath = path;
            //  PdfViewer2.LoadDocument(1);
            // PdfViewer1.LoadOnEnable = true;
        }
        /* text.text = "";
if (Application.isEditor)
{
 PdfViewer1.FilePath = Application.streamingAssetsPath + "/"+ defaultPDFFile;
 PdfViewer1.LoadOnEnable = true;
}
else
{
 var args = System.Environment.GetCommandLineArgs();
 if (args.Length > 1)
 {
     for (int i = 0; i < args.Length; i++)
     {
         text.text = text.text + args[i] + "\n";
         // if (args[i] == name && args.Length > i + 1)
         // {
         //     return args[i + 1];
         // }/
     }

     //return null;
     PdfViewer1.FilePath = args[1];
     PdfViewer1.LoadOnEnable = true;
 }  
}*/
        
    }
}